package com.reltio.lca;

/*
Enterprise Email Flag LCA
Stephen Knilans 5/2/2018
I estimate that this is about 90% done, The latest goals are:
1. Create a virtual crosswalk between the Fields, and contributors.
2. Create a list for external sources.
3. Add the capturedate and source checks.
4. Create the automated junit tests.

The structure, field retrieval and update(including crosswalks), and libraries should be done at this point.
This is saved only as an archive at the moment.
 */
import com.reltio.lifecycle.framework.*;
import org.apache.log4j.Logger;
import java.util.List;
import java.util.ListIterator;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.text.SimpleDateFormat;
//import java.util.Map;
//import java.util.HashMap;

public class BeforeSaveActionEnterpriseEmailFlag extends LifeCycleActionBase {

    public static final boolean DEBUG = false;
    private final Logger logger = Logger.getLogger(BeforeSaveActionEnterpriseEmailFlag.class.getName());
    final private String[] externalSources = new String[]{"MDR"};
    final private String emailTypes[] = {"WORK", "PERS", ""};  //Added third type, since Iknow it will be handy

    /*
     * Stephen Knilans
     * 4/23/2018
     * 
     * This scanEmailTypes scans only emails for a given type.  
     * 1. Emails that are not marked as optin or that have bounced, will have their enterpriseemailflag 
     * set to false.
     * 2. Emails that are later than the last candidate that are marked optin and have not bounced will
     * have the enterpriseemailflag set to true and be declared the last candidate, and the previous candidate 
     * will have its enterpriseemailflag set to false.  If the email is not considered a candidate, it will 
     * have its enterpriseemailflag set to false.
     * 
     * The end result will show any enterprise email, if any, and be returned to the scanEmailGroups method.
     * 
     */
    private ILifeCycleObjectData scanEmailType(int group1, String emailType1, ILifeCycleObjectData data) {

        // Setup an area to save the latest candidate date!
        String lastCandidate = null;
        String lastCandidateCapture = "";
        IAttributes LastCandidateAddress = null;
        String lastCandidateID = "";
        int group;
        // Get object from parameter
        IObject dataObject = data.getObject();
        /* intentionally set low as a default */
        Date CaptureDate;
        String CaptureDates;
        List<String> ACCaptureArray = new ArrayList<>();
        List<String> EXTCaptureArray = new ArrayList<>();
        //Map EXTCaptureArray = new HashMap();
        List<String> EXTSOURCE = Arrays.asList(externalSources);
        List<IAttributeValue> EnterpriseEmailFlagValues;
        INestedAttributeValue Emailn;
        String Email;
        String Type;
        String ThisSource;
        int useEXT = 0;
        ISimpleAttributeValue oldCandidateValue;
        String EEF;
        int isexternal;
        /*
        Code Added to handle CrossWalks
        Stephen Knilans 5/1/2018
         */

        try {
            ICrosswalks crosswalks = dataObject.getCrosswalks();
            if (crosswalks == null || crosswalks.getCrosswalks().isEmpty()) {
                /*
                This should also seriously speed up loads of Individuals, since this routing should have virtually no impact.
                 */
                //Gracefully degrade
            } else {
                List<ICrosswalk> crosswalksCopy = new ArrayList<>(crosswalks.getCrosswalks());
                for (ICrosswalk cw : crosswalksCopy) {
                    /*
                   Get meaningful data from Adobe Crosswalk
                   Stephen Knilans 5/2/2018
                     */
                    if (cw.getType().equals("configuration/sources/AdobeCampaign")) {
                        /*
                       We get the latest value of the record to get the last Capture Date
                       Stephen Knilans 5/2/2018
                         */
                        CaptureDate = cw.getCreateDate();
                        if (CaptureDate.compareTo(cw.getUpdateDate()) < 0) {
                            CaptureDate = cw.getUpdateDate();
                        }
                        String KeyValue = cw.getValue();
                        Integer Keyin = KeyValue.indexOf("|");
                        String KeyEmail;
                        /*
                      Only the capture date is remembered from the adobe crosswalk
                      Stephen Knilans 5/4/2018
                         */
                        if (Keyin > 0) {
                            KeyEmail = KeyValue.substring(0, Keyin);
                            SimpleDateFormat dt1 = new SimpleDateFormat("yyyy-MM-dd");
                            CaptureDates = dt1.format(CaptureDate);
                            //System.out.println("Capture date is: " + CaptureDates);
                            ACCaptureArray.add(KeyEmail + "|" + CaptureDates);
                        }
                    } else {
                        /*
                   Get URIS from NON Adobe Crosswalks that are EXTERNAL sources, so we can identify such email blocks
                   For Speed considerations, only the EMAIL address area will be considered
                   Stephen Knilans 5/2/2018
                         */
                        String datasrc = cw.getType();
                        ListIterator Sources = EXTSOURCE.listIterator();
                        isexternal = 0;
                        while (Sources.hasNext()) {
                            ThisSource = Sources.next().toString();
                            if (datasrc.equals("configuration/sources/" + ThisSource)) {
                                isexternal = 1;
                                break;
                            }
                        }
                        /*
                        There is no point in processing a crosswalk we don't care about
                        Stephen Knilans
                        5/4/2018
                         */
                        if (isexternal == 1) {
                            ICrosswalkAttributes ica = cw.getAttributes();
                            List icalist = ica.getAttributesURIs();
                            ListIterator icaitr = icalist.listIterator();
                            /*
                       Only External sources that deal with the email address will be recorded here
                       Stephen Knilans 5/4/2018
                             */
                            while (icaitr.hasNext()) {
                                String iscontributed = icaitr.next().toString();
                                if (iscontributed.indexOf("attributes/Email/") > 0) {
                                    //System.out.println("Contributed= " + iscontributed);
                                    EXTCaptureArray.add(iscontributed);
                                    //EXTCaptureArray.put(iscontributed,1);
                                }

                            }
                        }
                    }
                }
            }
        } catch (Exception ex) {
            String msg = ex.getMessage();
        }
        /*
        This is where we start processing the actual data
        Stephen Knilans
        5/4/2018
         */
        //Get attributes of object
        IAttributes attributes2 = dataObject.getAttributes();
        //Get values from attributes
        List<IAttributeValue> cmdidValues = attributes2.getAttributeValues("CMDID");
        ISimpleAttributeValue cmdid = (ISimpleAttributeValue) cmdidValues.get(0);
        String cmdidValue = cmdid.getStringValue();
        List<IAttributeValue> Email1Values = attributes2.getAttributeValues("Email");
        /*
        If there is no Email block, we will stop right here.
        This should also seriously speed up loads of Individuals, since this routing should have virtually no impact.
         */
        try {
            boolean Updated = false;
            if (!Email1Values.isEmpty()) {
                //System.out.println("Reading Email");
                /*
            Do all types as requested
                 */
                for (String emailType : emailTypes) {
                    /*
                Do external, and do external, if needed
                     */
                    EEF = "";
                    for (group = 0; group < 2; group++) {

                        /*
            Iterate through any arrays in the main Email Array
            Stephen Knilans 5/4/2018
                         */
                        for (IAttributeValue Email1Value : Email1Values) {

                            /*
                    Each Pass starts with the EnterpriseEmailFlag defaulting to not set.
                    Iterate through each email under the old email section.
                             */
                            Emailn = (INestedAttributeValue) Email1Value;

                            /*
                    Get the Email Block
                This is the main key to the block and to who gets the data
                             */
                            List<IAttributeValue> EmailnValueValues = Emailn.getValue().getAttributeValues("Email");
                            Email = "";
                            if (!EmailnValueValues.isEmpty()) {

                                Email = EmailnValueValues.get(0).getValue().toString();
                                //System.out.println("Get Nested Value " + Email);
                            }
                            /*
                    Get the Type
                This is part of the key to a block to be processed
                             */
                            List<IAttributeValue> TypeValues = Emailn.getValue().getAttributeValues("Type");
                            Type = "";
                            if (!TypeValues.isEmpty()) {
                                Type = TypeValues.get(0).getValue().toString();
                                //System.out.println("Get Nested Value " + Type);
                            }
                            /*
                    Get the URI Code
                This is the KEY to matching the crosswalks to the data.  
                PLEASE NOTE, this URI is to the Email address.  This is key
                             */
                            List<IAttributeValue> URIValues = Emailn.getValue().getAttributeValues("Email");
                            String MURI = "";
                            if (!URIValues.isEmpty()) {
                                MURI = URIValues.get(0).getUri();
                                //System.out.println("Get Nested Value " + MURI);
                            }
                            /*
                Please note, this is a part of the key to the processing.  Each type will be processed as if only this type exists
                             */
                            //System.out.println(Type + " ? " + emailType);
                            if (Type.equals(emailType)) {

                                ListIterator capitr = ACCaptureArray.listIterator();
                                CaptureDates = "";
                                String tempd = "";
                                while (capitr.hasNext()) {
                                    String KeyValue = capitr.next().toString();

                                    Integer Keyin = KeyValue.indexOf("|");
                                    if (KeyValue.length() > 1) {
                                        if (Keyin > 0) {
                                            String temp = KeyValue.substring(0, Keyin);
                                            String tempd2 = KeyValue.substring(Keyin + 1, KeyValue.length());
                                            if (temp.equals(Email)) {
                                                if (tempd2.compareTo(CaptureDates) > 0) {
                                                    CaptureDates = tempd2;
                                                }
                                            }
                                        }
                                    }
                                }

                                ListIterator FindEXT = EXTCaptureArray.listIterator();
                                isexternal = 0;
                                while (FindEXT.hasNext()) {
                                    if (FindEXT.next().toString().equals(MURI)) {
                                        //if (EXTCaptureArray.containsKey(MURI)){
                                        isexternal = 1;
                                        /*
                            We got our answer, so we will exit this loop
                                         */
                                        break;
                                    }
                                }
                                /*
                    This is another key, as the non external should set checked first, and then the external
                                 */
                                if (group == isexternal) {
                                    /*
                    Remove the current EnterpriseEmailFlags, if any
                                     */
                                    EnterpriseEmailFlagValues = Emailn.getValue().getAttributeValues("EnterpriseEmailFlag");
                                    while (!EnterpriseEmailFlagValues.isEmpty()) {
                                        //System.out.println("set EEF(after remove " + Email + ")=" + EEF);
                                        IAttributeValue oldValue = Emailn.getValue().getAttributeValues("EnterpriseEmailFlag").get(0);
                                        Emailn.getValue().removeAttributeValue(oldValue);
                                    }
                                    /*
                    Get the current Deliverability Status.
                    
                    If it is UNDELIVERABLE, set the EnterpriseEmailFlag to FALSE
                                     */
                                    List<IAttributeValue> DeliverabilityStatusValues = Emailn.getValue().getAttributeValues("DeliverabilityStatus");
                                    if (!DeliverabilityStatusValues.isEmpty()) {
                                        String DeliverabilityStatus = DeliverabilityStatusValues.get(0).getValue().toString();
                                        if (DeliverabilityStatus.equals("UNDELIVERABLE")) {
                                            EEF = "false";
                                        }
                                    }
                                    /*
                    Get the current Optin Status
                    
                    If it is false, set the EnterpriseEmailFlag to FALSE
                                     */
                                    EEF = "";
                                    List<IAttributeValue> OptInStatusFlagValues = Emailn.getValue().getAttributeValues("OptInStatusFlag");
                                    if (!OptInStatusFlagValues.isEmpty()) {
                                        String OptInStatusFlag = OptInStatusFlagValues.get(0).getValue().toString();
                                        /*
                    I have been told this should default to true, without adobe info, since they only state if someone opted out
                                         */
                                        if ((!OptInStatusFlag.equalsIgnoreCase("false")) && (!OptInStatusFlag.equalsIgnoreCase("true"))) {
                                            while (!OptInStatusFlagValues.isEmpty()) {
                                                //System.out.println("set EEF(after remove " + Email + ")=" + EEF);
                                                IAttributeValue oldValue = Emailn.getValue().getAttributeValues("OptInStatusFlag").get(0);
                                                Emailn.getValue().removeAttributeValue(oldValue);
                                            }
                                            oldCandidateValue = Emailn.getValue().createSimpleAttributeValue("OptInStatusFlag").value("true").build();
                                            Emailn.getValue().addAttributeValue(oldCandidateValue);
                                        } else if (OptInStatusFlag.equalsIgnoreCase("false")) {
                                            EEF = "false";
                                        }
                                    }
                                    /*
                    Get the optin Status Date
                                     */
                                    //String OptInStatusDate = "";
                                    //List<IAttributeValue> OptInStatusDateValues = Emailn.getValue().getAttributeValues("OptInStatusDate");
                                    //if (!OptInStatusDateValues.isEmpty()) {
                                    //  OptInStatusDate = OptInStatusDateValues.get(0).getValue().toString();
                                    //if (OptInStatusDate.compareTo(CaptureDates) > 0) {
                                    //    CaptureDates=OptInStatusDate;
                                    //}
                                    //}
                                    //We KNOW it is not a candidate, so save time by finishing NOW!
                                    if (EEF.equals("false")) {
                                        oldCandidateValue = Emailn.getValue().createSimpleAttributeValue("EnterpriseEmailFlag").value("false").build();
                                        Emailn.getValue().addAttributeValue(oldCandidateValue);
                                    } else {
                                        /*
                    Get the last acknowledge Date
                                         */
                                        String LastAcknowledgedDate;
                                        List<IAttributeValue> LastAcknowledgedDateValues = Emailn.getValue().getAttributeValues("LastAcknowledgedDate");
                                        if (!LastAcknowledgedDateValues.isEmpty()) {
                                            LastAcknowledgedDate = LastAcknowledgedDateValues.get(0).getValue().toString();
                                            if (LastAcknowledgedDate.compareTo(CaptureDates) > 0) {
                                                CaptureDates = LastAcknowledgedDate;
                                            }
                                        }
                                        /*
                    Get the Deliverability Status Date
                                         */
                                        String DeliverabilityStatusDate;
                                        List<IAttributeValue> DeliverabilityStatusDateValues = Emailn.getValue().getAttributeValues("DeliverabilityStatusDate");
                                        if (!DeliverabilityStatusDateValues.isEmpty()) {
                                            DeliverabilityStatusDate = DeliverabilityStatusDateValues.get(0).getValue().toString();
                                            if (DeliverabilityStatusDate.compareTo(CaptureDates) > 0) {
                                                CaptureDates = DeliverabilityStatusDate;
                                            }
                                        }
                                        //NOW, WE KNOW this really is not going to be a candidate, take care of it now.
                                        if ((lastCandidate != null) && (lastCandidate.compareTo(CaptureDates) > 0)) {
                                            oldCandidateValue = Emailn.getValue().createSimpleAttributeValue("EnterpriseEmailFlag").value("false").build();
                                            Emailn.getValue().addAttributeValue(oldCandidateValue);
                                        } else {
                                            /*
                                Stephen Knilans 5/9/2018
                                This is a VERY important note to remember:
                                
                                Due to Adobe campaign assuming that all is OPT-IN, there is NO OPT-IN date!
                                We only have an optout date.
                                
                                OPT-IN date is not looked at for this very reason.
                                             */


 /*
                    Update Latest EEF status, if TRUE
                    EEF.equals("") at this point means this is a candidate!
                                             */
                                            if (lastCandidate == null) {
                                                lastCandidate = "";
                                            }
                                            if (EEF.equals("")) {
                                                if (!(lastCandidateID.equals(""))) {
                                                    /*
                            We have to compare candidates first!
                                                     */

                                                    if (lastCandidate.compareTo(CaptureDates) < 0) {
                                                        /*
                               The current candidate is better, so invalidate the other one
                                                         */
                                                        if (LastCandidateAddress != null) {
                                                            oldCandidateValue = LastCandidateAddress.createSimpleAttributeValue("EnterpriseEmailFlag").value("false").build();
                                                            LastCandidateAddress.addAttributeValue(oldCandidateValue);

                                                            /*
                                  Setup New Candidate
                                                             */
                                                            lastCandidate = CaptureDates;
                                                            LastCandidateAddress = Emailn.getValue();
                                                            lastCandidateID = Email;

                                                        } else {
                                                            lastCandidate = CaptureDates;
                                                            LastCandidateAddress = Emailn.getValue();
                                                            lastCandidateID = Email;
                                                        }

                                                    } else if (lastCandidate.compareTo(CaptureDates) >= 0) {
                                                        oldCandidateValue = Emailn.getValue().createSimpleAttributeValue("EnterpriseEmailFlag").value("false").build();
                                                        Emailn.getValue().addAttributeValue(oldCandidateValue);
                                                    }
                                                } else {
                                                    lastCandidate = CaptureDates;
                                                    LastCandidateAddress = Emailn.getValue();
                                                    lastCandidateID = Email;

                                                }
                                            }
                                        }
                                    }
                                }/* limit to group conditional */
 /*
                    Update the EnterpriseEmailFlag
                    
                    This initial stage only leaves possibly valid flags set to nothing, and sets bad ones to false
                                 */


                                //System.out.println("Current EnterpriseEmailFlag " + Email + "=" + EnterpriseEmailFlag);
                            }
                            /* Done looking at emails for a group/type */

                        }
                        /*
                    Get the EnterpriseEmailFlag value from the actual object!
                         */
 /*EnterpriseEmailFlagValues = Emailn.getValue().getAttributeValues("EnterpriseEmailFlag");
                    String EnterpriseEmailFlag = "false";
                    if (!EnterpriseEmailFlagValues.isEmpty()) {
                        EnterpriseEmailFlag = EnterpriseEmailFlagValues.get(0).getValue().toString();
                    } */

                        // System.out.println("FINISH ENTERPRISE");
                        /* Finish Enterprise Email */
//                    EnterpriseEmailFlagValues = Emailn.getValue().getAttributeValues("EnterpriseEmailFlag");
//                    if (!EnterpriseEmailFlagValues.isEmpty()) {
//                        //System.out.println("set EEF(after remove " + Email + ")=" + EEF);
//                        IAttributeValue oldValue = Emailn.getValue().getAttributeValues("EnterpriseEmailFlag").get(0);
//                        Emailn.getValue().removeAttributeValue(oldValue);
//                    }
                        if (!lastCandidateID.equals("")) {
                            //System.out.println("set " + Email + " EEF=" + EEF);
                            if ((group == 1) && (useEXT == 0)) {
                                useEXT = 1;
                            }
                            /*
                                        If we are processing an external source, and the internalsource got a good EE, then we will set this to FALSE!
                             */
                            //    EEF = "false";
                            //    lastCandidateID = "";
                            //} else {
                            //    EEF = "true";
                            //}
                            //if (!Emailn.getValue().equals(LastCandidateAddress)) {
                            //    ISimpleAttributeValue newValue = Emailn.getValue().createSimpleAttributeValue("EnterpriseEmailFlag").value(EEF).build();
                            //    Emailn.getValue().addAttributeValue(newValue);
                            //}
                            if (!(lastCandidateID.compareTo(" ") <= 0)) {
                                if (LastCandidateAddress != null) {
                                    oldCandidateValue = LastCandidateAddress.createSimpleAttributeValue("EnterpriseEmailFlag").value("true").build();
                                    LastCandidateAddress.addAttributeValue(oldCandidateValue);
                                }
                                useEXT = 0;
                                //lastCandidateID = Email;
                            }
                        }
                        /* Check Last candidate address */
 /* Finish Enterprise Email */

                        lastCandidateID = "";
                        LastCandidateAddress = null;
                        EEF = "";

                    }
                    /* Done Looking at Groups */
                }
                /* Done Looking at Email Types */
            }
            /* Done looking at Email Values for this pass */
            Updated = true;
            return data;
        } catch (Exception ex) {
            //reltioAPI.logError(ex.getMessage());
            throw new RuntimeException("BeforeSaveActionEnterpriseEmailFlag: LCA invokation failed.");
        }
    }

    /*
	 * Stephen Knilans
	 * 4/23/2018
	 * 
	 * This scanEmailGroups provides THREE functions:
	 * 1. Each type is scanned in turn.  A type declares where it is to be used, such as 
	 * work or home.
	 * 2. The data is then sent to scanEmailType to scan the type using only internal data.
	 * 3. If scanEmailType returns a 0, no enterpriseemailflag could be set, and it is scanned 
	 * using only external data("MDR").
	 * 
	 * The end result will show any enterprise email, if any, and be returned to the beforeSave method.
	 * 
     */
 /*
    Written by Stephen Knilans
    04/30/2018
    
    This process is to determine the EnterpriseEmail in an individual that is saved in Reltio at Scholastic.
    Any item that is undeliverable is set to NOT be an Enterprise Email
    Any Item that has NOT been opted in is set to NOT be an Enterprise Email
    Any remainders that have an optin date that is not higher than the last candidate are set to NOT be an Enterprise Email.
    The remaining item, if any, is set to be an enterprise email.  This is equivalent to the last candidate, and that group
    of values is used to set it to increase performance, by doing all of the above in one pass.
    
     */
    @Override
    public ILifeCycleObjectData beforeSave(IReltioAPI reltioAPI, ILifeCycleObjectData data) {
        ILifeCycleObjectData data2 = scanEmailType(0, "", data);
        return data2;

    }

}
