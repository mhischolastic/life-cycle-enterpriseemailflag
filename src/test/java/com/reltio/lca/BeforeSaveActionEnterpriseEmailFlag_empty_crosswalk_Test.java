package com.reltio.lca;

import com.fasterxml.reltio.jackson.databind.JsonNode;
import com.fasterxml.reltio.jackson.databind.ObjectMapper;
import com.google.common.io.Files;
import com.reltio.lifecycle.server.core.services.LifeCycleHook;
import com.reltio.lifecycle.test.LifecycleExecutor;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.Charset;

import static org.junit.Assert.assertEquals;

public class BeforeSaveActionEnterpriseEmailFlag_empty_crosswalk_Test {
    @Test
    public void test() throws URISyntaxException, IOException {
        BeforeSaveActionEnterpriseEmailFlag handler = new BeforeSaveActionEnterpriseEmailFlag();
        LifecycleExecutor executor = new LifecycleExecutor();
        String input = Files.toString(new File(getClass().getResource("/EnterpriseEmail_empty_crosswalk_Test.json").toURI()), Charset.defaultCharset());

        String result = executor.executeAction(handler, LifeCycleHook.beforeSave, input);

        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode jsonNode = objectMapper.readValue(result, JsonNode.class);
        //String name = jsonNode.get("object").get("attributes").toString();
//                .elements().next().get("value").asText();
        //System.out.println(name);
        //assertEquals("John Snow", name);
    }
}
