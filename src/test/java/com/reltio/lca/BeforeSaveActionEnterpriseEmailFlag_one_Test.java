package com.reltio.lca;

import com.fasterxml.reltio.jackson.databind.JsonNode;
import com.fasterxml.reltio.jackson.databind.ObjectMapper;
import com.google.common.io.Files;
import com.reltio.lifecycle.server.core.services.LifeCycleHook;
import com.reltio.lifecycle.test.LifecycleExecutor;
import org.junit.Test;
import org.skyscreamer.jsonassert.*;
import org.json.JSONException;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import static org.skyscreamer.jsonassert.JSONCompareMode.LENIENT;

//import static org.junit.Assert.assertEquals;

public class BeforeSaveActionEnterpriseEmailFlag_one_Test {
    @Test
    public void test() throws URISyntaxException, IOException {
        BeforeSaveActionEnterpriseEmailFlag handler = new BeforeSaveActionEnterpriseEmailFlag();
        LifecycleExecutor executor = new LifecycleExecutor();
        String input = Files.toString(new File(getClass().getResource("/EnterpriseEmail_one_Test.json").toURI()), Charset.defaultCharset());
        String expected = Files.toString(new File(getClass().getResource("/EnterpriseEmail_one_Result.json").toURI()), Charset.defaultCharset());
        String result = executor.executeAction(handler, LifeCycleHook.beforeSave, input);
        if (false){
           ObjectMapper objectMapper = new ObjectMapper();
           JsonNode jsonNode = objectMapper.readValue(result, JsonNode.class);
           String name = jsonNode.toString();
           System.out.println(name);
        }
        try {
        JSONAssert.assertEquals(expected, result, LENIENT);
        } catch (JSONException ex){
            System.out.println("FAILURE OCCURED");
        }


//                .elements().next().get("value").asText();
        //
        //assertEquals("John Snow", name);
    }
}
